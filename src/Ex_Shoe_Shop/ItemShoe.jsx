import React, { Component } from 'react'

export default class ItemShoe extends Component {
    render() {
        let { name, image, price } = this.props.item
        return (
            <div className="col-3 p-3">
                <div className="card">
                    <img className="card-img-top" src={image} alt="Card image cap" />
                    <div className="card-body">
                        <h5 className="card-title">{name}</h5>
                        <p className="card-text">{price} $</p>
                        <a onClick={() => { this.props.handleOnclick(this.props.item) }} href="#" className="btn btn-primary">Add to cart</a>
                    </div>
                </div>
            </div>
        )
    }
}
