import React, { Component } from 'react'

export default class Cart extends Component {
    renderTbody = () => {
        return this.props.cart.map((item) => {
            return (<tr>
                <td>{item.id}</td>
                <td>{item.name}</td>
                <td>{item.price * item.soLuong} $</td>
                <td><img src={item.image} alt="" style={{ width: 50 }} /></td>
                <td>
                    <button onClick={() => { this.props.handleChangeQuantity(item.id, '-1') }} className="btn btn-warning">-</button>
                    <strong className='mx-3'>{item.soLuong}</strong>
                    <button onClick={() => { this.props.handleChangeQuantity(item.id, '1') }} className="btn btn-warning">+</button>
                </td>
                <td><button onClick={() => { this.props.handleRemove(item.id) }} className='btn btn-danger'>Delete</button></td>
            </tr>)
        })
    }
    render() {
        return (
            <div>
                <table className="table">
                    <thead>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Image</th>
                        <th></th>
                    </thead>
                    <tbody>
                        {this.renderTbody()}
                    </tbody>
                </table>
            </div>
        )
    }
}
